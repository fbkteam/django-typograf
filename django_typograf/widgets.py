# -*- coding: utf-8 -*-

from django import forms
from django.forms.utils import flatatt
from django.utils.encoding import force_text
from django.utils.html import format_html


class TypografWidget(forms.widgets.Textarea):
    def render(self, name, value, attrs=None, kwargs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, {'name':name})
        final_attrs['class'] = ' '.join([final_attrs.get('class', ''), 'vLargeTextField'])
        return format_html(u'<textarea{}>\r\n{}</textarea>', flatatt(final_attrs), force_text(value)) + u'''
        <script>
            var input = $('#%s')[ 0 ];
            var inlineTypograf = new TypografInput({input: input });
            inlineTypograf.$autoButton.text('Авто');
            if(inlineTypograf.auto){
              inlineTypograf.$autoButton.addClass('tp--auto-on');
            } else {
              inlineTypograf.$autoButton.addClass('tp--auto-off');
            };
            inlineTypograf.$applyButton.text('Типографировать');
            inlineTypograf.$buttons.insertAfter(input);
        </script>
        ''' % (final_attrs['id'])

    class Media:
        js = (
            'django-typograf/js/vendor/jquery/jquery-3.1.1.min.js',
            'django-typograf/js/TypografInput.js',
        )
        css = {
            'all': ('django-typograf/css/typograf.css',)
        }


class TypografWidgetInput(forms.widgets.TextInput):
    def render(self, name, value, attrs=None, kwargs=None):

        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, {'type': self.input_type, 'name': name})
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_text(self._format_value(value))
        final_attrs['class'] = ' '.join([final_attrs.get('class', ''), 'vLargeCharField'])
        return format_html(u'<input{} />', flatatt(final_attrs)) + u'''
        <script>
            var input = $('#%s')[ 0 ];
            var inlineTypograf = new TypografInput({input: input });
            inlineTypograf.$autoButton.text('Авто');
            if(inlineTypograf.auto){
              inlineTypograf.$autoButton.addClass('tp--auto-on');
            } else {
              inlineTypograf.$autoButton.addClass('tp--auto-off');
            };
            inlineTypograf.$applyButton.text('Типографировать');
            inlineTypograf.$buttons.insertAfter(input);
        </script>
        ''' % (final_attrs['id'])

    class Media:
        js = (
            'django-typograf/js/vendor/jquery/jquery-3.1.1.min.js',
            'django-typograf/js/TypografInput.js',
        )
        css = {
            'all': ('django-typograf/css/typograf.css',)
        }
