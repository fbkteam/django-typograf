import os
from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='django-typograf',
    version='0.1',
    description='Typograf form widgets for text fields',
    long_description=readme(),
    packages=find_packages(),
    url='https://bitbucket.org/fbkteam/django-typograf.git',
    author='Fbkteam',
    include_package_data=True,
)
