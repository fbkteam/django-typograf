# Django typograf

Typograf fields form widgets


## Install

```
pip install git+https://bitbucket.org/fbkteam/django-typograf.git
```


## Quick start

1. Add "django-typograf" to your INSTALLED_APPS setting like this:
    
        :::python
        # settings.py
        
        INSTALLED_APPS = [
            ...
            'django_typograf',
        ]

2. Now we can use typograf widgets in forms.

* One way to do that - set custom widgets to all form fields by types using [formfield_overrides](https://docs.djangoproject.com/en/1.11/ref/contrib/admin/#django.contrib.admin.ModelAdmin.formfield_overrides):

        :::python
        # admin.py
        
        from django.db import models
        from django.contrib import admin
        from django_typograf.widgets import TypografWidget, TypografWidgetInput

        from .models import Post


        @admin.register(Post)
        class PostAdmin(admin.ModelAdmin):
            formfield_overrides = {
                models.CharField: {'widget': TypografWidgetInput},
                models.TextField: {'widget': TypografWidget},
            }

* Another, more flexible solution - set widgets in admin form class by field name:

        :::python
        # forms.py
        
        from django import forms
        from django_typograf.widgets import TypografWidget, TypografWidgetInput
        
        from .models import Post
        
        
        class PostAdminForm(forms.ModelForm):
            class Meta:
                model = Post
                fields = '__all__'
                widgets = {
                    'title': TypografWidgetInput, 
                    'description': TypografWidget
                }
    
    And set your custom form in admin class:

        :::python
        # admin.py
        
        from django.contrib import admin
        
        from .models import Post
        from .forms import PostAdminForm
        
        
        @admin.register(Post)
        class PostAdmin(admin.ModelAdmin):
            form = PostAdminForm
